# TPO_Codo_A_Codo
Trabajo Práctico Obligatorio
Agustina Naccarato

Página que brinda ayuda al interrogante diario "¿qué puedo comer hoy?", contemplando sugerencias en base al clima de su ciudad actual o bien según disponibilidad de alimentos. Además cuenta con calculadoras de cocina para facilitar la labor.  
